# Yoloco test task
## Goal
Search cities on Wikipedia and return info about them (population, indexes, country, country subject etc.)

## How to launch
1. Clone the repository
2. `docker-compose up`
3. Server will be available at `localhost:8000`

## Endpoints
Two endpoints:

`GET city/search/?query=<your-query>` - search the Wiki for an entry. Returns all variants that are like your query.

`GET city/summary/?city=<your-city>` - search the <your-city> Wiki page and return all the information about it (from the Infobox)

## Packages used
1.Django

2.Django Rest Framework

3.Wikipedia

4.BeautifulSoup4
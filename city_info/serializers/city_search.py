from rest_framework import serializers
from rest_framework.exceptions import NotFound
from wikipedia import PageError

from city_info.services.city_search import CitySearchService


class CitySearchSerializer(serializers.Serializer):
    query = serializers.CharField()

    def create(self, validated_data):
        query = validated_data["query"]
        return CitySearchService().search(query)


class CitySummarySerializer(serializers.Serializer):
    city = serializers.CharField()

    def create(self, validated_data):
        city = validated_data["city"]
        try:
            return CitySearchService().summary(city)
        except PageError:
            raise NotFound(detail="Город не найден! Попробуйте изменить запрос.")

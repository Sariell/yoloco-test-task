import re
import ssl
from urllib import request

import wikipedia
from bs4 import BeautifulSoup
from rest_framework.exceptions import NotFound


class CitySearchService:
    def search(self, query):
        """Поиск нужной статьи"""
        wikipedia.set_lang("ru")
        return {"results": wikipedia.search(query)}

    def summary(self, city):
        """Информация о статье (берется из инфобокса, который находится справа на википедии)"""
        # узнаем урл нужной нам страницы (сама страница не запрашивается)
        wikipedia.set_lang("ru")
        page = wikipedia.page(city)
        site = page.url

        # запрашиваем страницу
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = request.Request(site, headers=hdr)
        gcontext = ssl.SSLContext()
        page = request.urlopen(req, context=gcontext).read()

        # оптимизация: ищем во всей странице только инфобокс и отбрасываем лишнюю часть строки
        # (для данной задачи все страницы имеют статичный лейаут)
        page = page.decode("UTF-8")
        try:
            left = page.index('<table class="infobox')
            right = page.index('</table>', left) + len('</table>')
        except ValueError:
            raise NotFound(detail="Нет информации по данному городу (infobox отсутсвует)")
        div = page[left:right]

        closed_table_count = 0
        open_table_cout = 1
        while closed_table_count != open_table_cout:
            open_table_cout = len(re.findall('<table', div))
            closed_table_count = len(re.findall('</table>', div))
            right = page.index('</table>', right) + len('</table>')
            div = page[left:right]

        # преобразуем в суп и парсим
        soup = BeautifulSoup(div, features='html.parser')
        # table = soup.find('table', class_='infobox vcard')
        result = {}
        for tr in soup.find_all('tr'):
            if tr.find('th') and tr.find('td'):
                # чистим данные
                value = tr.find('td').text.replace("\n", "").replace("", "")
                value = re.sub("\\[.*?\\]", "", value)

                result[tr.find('th').text] = value

        return result

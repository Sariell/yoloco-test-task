from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from city_info.serializers.city_search import (
    CitySearchSerializer,
    CitySummarySerializer,
)


class CitySearchViewSet(GenericViewSet):
    """Сначала сделал APIView, но потом решил добавить эндпоинт поиска статьи, стало оптимальнее
    использовать GenericViewSet, чтобы в нем были кастомные action'ы (и только они)."""

    @action(detail=False, methods=["GET"])
    def search(self, request):
        serializer = CitySearchSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)

    @action(detail=False, methods=["GET"])
    def summary(self, request):
        serializer = CitySummarySerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)
